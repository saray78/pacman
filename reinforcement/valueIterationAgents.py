# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        
        ############Inicializar#####################
        values2 = self.values.copy()
        state = self.mdp.getStates()
        
        # Para cada iteracion y si no es terminal:
        #1. calcula las acciones (politicas)
        #2 Si la accion no es nula--> calcular los valores optimos
        for it in range(iterations):
        	for s in state:
        		if  not self.mdp.isTerminal(s):
        			action = self.computeActionFromValues(s)
        			if action is not None:
        				values2[s] = self.computeQValueFromValues(s,action)

        	for s in state:
        			#Actualizar
        			self.values[s]  = values2[s]

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        #####Formula#######
        #Q*(s,a) = suma Pa(s'|s)[R(s,a,s') + discount V *(s')]
        
        #Inicializar
        qValue = 0
        transition = self.mdp.getTransitionStatesAndProbs(state,action)
        
        #recorrer transicion
        #Siguiente transicion y probabilidad
        #Calculo de Q*
        for s2, prob in transition:
        	reward = self.mdp.getReward(state,action,s2)
        	qValue += prob * (reward  + self.discount * self.values[s2])
        	
        return qValue
        
        
        #util.raiseNotDefined()

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        
        #############declarar#################
        actions = self.mdp.getPossibleActions(state)
        value = -float("Inf")
        bestAction = None
        
        #Cuando no haya acciones legales
        if len(actions) == 0:
        	return None
        
        #Para cada accion que haya
        #calculo de Qvalue del estado actual segun accion escogida
        #Si es mejor actualizar	
        for action in actions:
        	qValue = self.computeQValueFromValues(state,action)
        	
        	if qValue > value or value == None:
        		value = qValue
        		bestAction = action
        	
        return bestAction
        

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
