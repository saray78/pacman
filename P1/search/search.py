# -*- coding: utf-8 -*-
# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    
    #declaracion variables del nodo: 
    estado = problem.getStartState()
    ultimaAccion = ''
    ganancia = 0
    nodoPadre = None
    # Estruct nodo inicial
    Ninicial = (estado,ultimaAccion,ganancia, nodoPadre)
    
    #Seguimos algoritmo general
    #Declaramos una pila(Open) utilizando la funcion definida de util.py
    Op = util.Stack()
    
    #Declaramos un vector closed para saber los visitados
    closed = set()
    
    #Anadimos a la pila un nodo inicial
    Op.push(Ninicial)
    
    #Hacemos una comprobacion de open (frontera)
    while not Op.isEmpty():
        
        nodos = Op.pop() #Quita ultimo item de la lista
        #Si el estado es goal retornamos la solucion
        if problem.isGoalState(nodos[0]):
            #rellenar el camino
            solucion = []
            #
            while nodos[3]!= None:
                solucion.append(nodos[1]) #agrega item al final de la lista
                nodos = nodos[3]
            #damos la vuelta al camino
            solucion.reverse()
            return solucion
        else:
            # sino expandimos el nodo y calculamos los sucesores
            if not nodos[0] in closed:
                sucesores = problem.getSuccessors(nodos[0]) #obtenemos sucesores
                for suc in sucesores:
                    if not suc[0] in closed: #Si no se han visitado
                        #Calculo de los nodos hijo
                        estado = suc[0]
                        ultimaAccion = suc[1]
                        ganancia = nodos[2]+ suc[2]
                        nPadre = nodos
                        nFinal = (estado,ultimaAccion,ganancia,nPadre)#nodo resultante para poder ponerlo en la pila
                        
                        
                        #añadimos los sucesores a open como una pila con valores
                      
                        Op.push(nFinal)
                        
        closed.add(nodos[0])#hijos del nodo al principio del nodo
    #Si no hay nada en open, lo retornara vacio
    return []
    #util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    
    #declaracion variables del nodo: 
    estado = problem.getStartState()
    ultimaAccion = ''
    ganancia = 0
    nodoPadre = None
    Ninicial = (estado,ultimaAccion,ganancia, nodoPadre)
    
    #Seguimos algoritmo general
    #Declaramos una pila (Open) utilizando la funcion definida de util.py
    Op = util.Queue()
    
    #Declaramos un vector closed para saber los visitados
    closed = set()
    
    #Anadimos un nodo inicial con estado,ultima accion realizado,coste y padre
    Op.push(Ninicial)
    
    #Hacemos una comprobacion de open (frontera)
    while not Op.isEmpty():
        #
        nodos = Op.pop()
        #Si es goal retornamos la solucion
        if problem.isGoalState(nodos[0]):
            #rellenar el camino
            solucion = []
            #
            while nodos[3]!= None:
                solucion.append(nodos[1])
                nodos = nodos[3]
            #damos la vuelta al camino
            solucion.reverse()
            return solucion
        else:
            # sino expandimos el nodo y calculamos los sucesores
            if not nodos[0] in closed:
                sucesores = problem.getSuccessors(nodos[0]) #obtenemos sucesores
                for suc in sucesores:
                    if not suc[0] in closed: #Si no se han visitado
                        
                        #Calculo de los nodos hijo
                        estado = suc[0]
                        ultimaAccion = suc[1]
                        ganancia = nodos[2]+ suc[2]
                        nPadre = nodos
                        nFinal = (estado,ultimaAccion,ganancia,nPadre)#nodo resultante para poder ponerlo en la pila
                        
                        
                        #añadimos los sucesores a open como una pila con valores
                        
                        Op.push(nFinal)
        #hijos de ultimos                
        closed.add(nodos[0])
    #Si no hay nada en open, lo retornara vacio
    return []

    
    #util.raiseNotDefined()

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    
    #declaracion variables del nodo: 
    estado = problem.getStartState()
    ultimaAccion = ''
    ganancia = 0
    pCola = ganancia #En coste uniforme seran iguales
    nodoPadre = None
    Ninicial = (estado,ultimaAccion,ganancia, nodoPadre)
    
    #Seguimos algoritmo general
    #Declaramos una cola de prioridad (Open) utilizando la funcion definida de util.py
    Op = util.PriorityQueue()
    
    #Declaramos un vector closed para saber los visitados
    closed = set()
    
    #Anadimos un nodo inicial con estado,ultima accion realizado,coste, prioridad de la cola y padre
    Op.push(Ninicial,pCola)
    
    #Hacemos una comprobacion de open (frontera)
    while not Op.isEmpty():
        #
        nodos = Op.pop()
        #Si es goal retornamos la solucion
        if problem.isGoalState(nodos[0]):
            #rellenar el camino
            solucion = []
            #Si no es el padre
            while nodos[3]!= None:
                solucion.append(nodos[1]) #añadir pasos
                nodos = nodos[3]#llegas al final
            #damos la vuelta al camino
            solucion.reverse()
            return solucion
        else:
            # sino expandimos el nodo y calculamos los sucesores
            if not nodos[0] in closed:
                sucesores = problem.getSuccessors(nodos[0]) #obtenemos sucesores
                for suc in sucesores:
                    if not sucesores[0] in closed: #Si no se han visitado
                        #Calculo de los nodos hijo
                        estado = suc[0]
                        ultimaAccion = suc[1]
                        ganancia = nodos[2]+ suc[2]
                        pCola = ganancia
                        nPadre = nodos
                        nFinal = (estado,ultimaAccion,ganancia,nPadre)#nodo resultante para poder ponerlo en la pila
                        
                        
                        #añadimos los sucesores a open como una cola con prioridad con valores
                        
                        Op.push(nFinal,pCola)
                        
        closed.add(nodos[0])
    #Si no hay nada en open, lo retornara vacio
    return []


    
    #util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    estado = problem.getStartState()
    ultimaAccion = ''
    ganancia = 0
    #heuristica = heuristic(estado,problem)
    pCola = ganancia + heuristic(estado,problem) #calculo de la cola de prioridades 
    nodoPadre = None
    Ninicial = (estado,ultimaAccion,ganancia, nodoPadre)
    
    #Seguimos algoritmo general
    #Declaramos una cola con prioridad (Open) utilizando la funcion definida de util.py
    Op = util.PriorityQueue()
    
    #Declaramos un vector closed para saber los visitados
    closed = set()
    
    #Anadimos un nodo inicial con estado,ultima accion realizado,coste y padre
    Op.push(Ninicial,pCola)
    
    #Hacemos una comprobacion de open (frontera)
    while not Op.isEmpty():
        #
        nodos = Op.pop()
        #Si es goal retornamos la solucion
        if problem.isGoalState(nodos[0]):
            #rellenar el camino
            solucion = []
            #Si no es el padre
            while nodos[3]!= None:
                solucion.append(nodos[1])
                nodos = nodos[3]
            #damos la vuelta al camino
            solucion.reverse()
            return solucion
        else:
            # sino expandimos el nodo y calculamos los sucesores
            if not nodos[0] in closed:
                sucesores = problem.getSuccessors(nodos[0]) #obtenemos sucesores
                for suc in sucesores:
                    if not suc[0] in closed: #Si no se han visitado
                        #Calculo de los nodos hijo
                        estado = suc[0]
                        ultimaAccion = suc[1]
                        ganancia = nodos[2]+ suc[2]
                        pCola = ganancia + heuristic(estado,problem)
                        nPadre = nodos
                        nFinal = (estado,ultimaAccion,ganancia,nPadre)#nodo resultante para poder ponerlo en la pila
                        
                        
                        #añadimos los sucesores a open como una pila con valores
                        #estado,ultima accion realizada, ganancia y nodo padre
                        Op.push(nFinal,pCola
                                )
                        
        closed.add(nodos[0])
    #Si no hay nada en open, lo retornara vacio
    return []

    
    #util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
