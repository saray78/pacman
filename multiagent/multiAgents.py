# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
	#print currentGameState.generatePacmanSuccessor(action)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
	#print successorGameState.getPacmanPosition()
        newPos = successorGameState.getPacmanPosition()#pos pacman
	#print successorGameState.getFood()
        newFood = successorGameState.getFood()
	#print successorGameState.getGhostStates()
        newGhostStates = successorGameState.getGhostStates()
	#print [ghostState.scaredTimer for ghostState in newGhostStates]
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]#numero movimientos fantasma asustado

        "*** YOUR CODE HERE ***"
	
	###################Declarar variables#############################
	eat = currentGameState.getFood().asList()
	
	#dos tipos de fantasma: scared, dangerous
	scared = []
	dangerous = []
	scored1 = successorGameState.getScore()#coger pos actual
	
	#Recorrer las posiciones del fantasma
	for ghost in newGhostStates:
		ghostPos = ghost.getPosition()
		if ghost.scaredTimer > 0:# si es asustado
			scared.append(ghostPos)
		else:
			dangerous.append(ghostPos)#sera peligroso

	#Situaciones a comprobar:
	#1. si se encuentra  con fantasma que mata
	#2. si se para
	#3. calcular dist. Manhattan a comida

	#1.
	if newPos in dangerous:
		scored = -float("Inf")
		return scored
		
	#2.
	if action == Directions.STOP:
		scored = -float("Inf")
		return scored
		
	#3.
	scored2 = 0
	dist = []
	
	for eats in eat:
		dist.append(util.manhattanDistance(eats,newPos))
	if len(dist)> 0: 
		scored2 = min(dist)
	
	scored = -1*scored2
	return scored
	
def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        
        ###################Inicializar#############################
        agentIndex = 0
        depth = self.depth
        gameState = gameState
        
        #retornar accion
        scored, action = self.minimax(gameState, agentIndex, depth)
        return action
        
        
     ##########################Funcion mini max#####################
    def minimax(self, gameState, agentIndex, depth):
    
    	#tenemos que mirar si el juego ha llegado al fin (estados terminales)
    	##Devolver puntuacion y accion
    	if gameState.isWin() or gameState.isLose() or depth == 0:
    		scored = self.evaluationFunction(gameState)
    		action = ''
    		return (scored, action)
        
        #casos
        #1.Max
        #2.Min
        
        nextAgent = (agentIndex + 1) % gameState.getNumAgents()
        
        #Si somos el ultimo
        if nextAgent  == 0:
        	depth = depth - 1 #bajamos profundidad
        
        #Cuando sea Max elegimos al hijo con v max
        #Declarar valor como tupla: parte numerica y accion
        valor = -float("Inf"), None
        
        if agentIndex == 0:
        	for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored= self.minimax(estadoH, nextAgent, depth)[0]
        		valor = max(valor, (scored, action) )#tupla
        	return valor
        
        
        #Cuando sea MIN elegir hijo con valor minimo: fantasma
        else:
        	valor = float("Inf"), None #valor y accion
        	
        	for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored = self.minimax(estadoH, nextAgent, depth)[0]
        		valor = min(valor, (scored, action) )
			
        	return valor
        
        
        #util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        ###################Inicializar#############################
	agentIndex = 0
        depth = self.depth
        gameState = gameState
	#declaramos alfa y beta
	alfa = (-float("Inf"), '')
	beta = (float("Inf"),'')
	
	#retornar accion
        scored, action = self.AlphaBetaPruning(gameState, agentIndex, depth, alfa, beta)
        return action

    ####################Funcion alpha beta pruning##################	
    def AlphaBetaPruning(self, gameState, agentIndex, depth, alfa, beta):
    
    	#tenemos que mirar si el juego ha llegado al fin (estados terminales)
    	##Devolver puntuacion y accion
	if gameState.isWin() or gameState.isLose() or depth == 0:
    		scored = self.evaluationFunction(gameState)
    		action = ''
    		return (scored, action)
        
	#casos
        #1.Max
        #2.Min
        
        nextAgent = (agentIndex + 1) % gameState.getNumAgents()#variar los agentes, cambia
        
        #Si somos el ultimo
        if nextAgent== 0:
        	depth = depth - 1#bajamos profundidad
        
        #Cuando sea Max elegimos al hijo con v max
        #Declarar valor como tupla: parte numerica y accion
        valor = -float("Inf"), None
        
        if agentIndex == 0:
		for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored = self.AlphaBetaPruning(estadoH, nextAgent, depth, alfa, beta)[0]
        		valor = max(valor, (scored, action) )
			if valor>beta:
				return valor
			alfa = max(alfa,valor)
        	return valor

	
        
	#Cuando sea MIN elegir hijo con valor minimo: fantasma
        else:
        	valor = float("Inf"), None #valor y accion
        	
        	for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored = self.AlphaBetaPruning(estadoH, nextAgent, depth, alfa, beta)[0]
        		valor = min(valor, (scored, action) )
			if valor < alfa:
				return valor
			beta = min(beta, valor)
        	return valor
	
        #util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        ###################Inicializar#############################
        agentIndex = 0
        depth = self.depth
        gameState = gameState
        
        #retornar accion
        scored, action = self.expectimax(gameState, agentIndex, depth)
        return action
        
    ################Funcion expectimax##############################
    def expectimax(self, gameState, agentIndex, depth):
    
    	#tenemos que mirar si el juego ha llegado al fin (estados terminales)
    	##Devolver puntuacion y accion
    	if gameState.isWin() or gameState.isLose() or depth == 0:
    		scored = self.evaluationFunction(gameState)
    		action = ''
    		return (scored, action)
        
        #Casos
        #1.Max
        #2.Media--> es un nodo de probabilidad incierto, calculo valor esperado
        
        #Siguiente agente
        nextAgent = (agentIndex + 1) % gameState.getNumAgents()
    
        
        if nextAgent  == 0:
        	depth = depth - 1 #bajamos profundidad
        
        #Acumulara valores maximos para la media
        #Declarar valor como tupla: parte numerica y accion
        valor = -float("Inf"), None 
        
        if agentIndex == 0:
        	for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored= self.expectimax(estadoH, nextAgent, depth)[0]
        		valor = max(valor, (scored, action) )
        	return valor
        
        
        #Calculo valor esperado de score sucesores: media valores acciones posibles
        else:
        	valorE = 0
        	prob = 1.0/float(len(gameState.getLegalActions(agentIndex)))
        	
        	for action in gameState.getLegalActions(agentIndex):
        		estadoH = gameState.generateSuccessor(agentIndex, action)
        		scored = self.expectimax(estadoH, nextAgent, depth)[0]
        		valorE += scored *prob
        		#valorE/prob
        	valor = (valorE, '')
        	return valor
        	
        #util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    ##########variables necesarias##########################
    newPos = currentGameState.getPacmanPosition()#pos pacman
    eat = currentGameState.getFood().asList()
    scared = []
    dangerous = []
    
    ###########Recorrer posiciones fantasma#################
    ###########asustado y peligroso######################### 
    for ghost in currentGameState.getGhostStates():
    	ghostPos = ghost.getPosition()
    	if ghost.scaredTimer > 0:
    		scared.append(ghostPos)
    	else:
    		dangerous.append(ghostPos)
    		
    ##########Situaciones###################################### 
    #0. Si se encuentra con fantasma que mata
    #1. Puntuacion1 estado actual--> +7
    #2. Puntuacion2 fantasma peligroso mas cercano--> -1
    #5. Puntuacion5 comida cercana --> -1
    
    #0.		
    if newPos in dangerous:
    	scored = -float("Inf")
    	return scored
    	
    #1.	
    scored1 = currentGameState.getScore()
    
    #2.
    dist = []
    scored2 = 0	
    for ghost in dangerous:
    	dist.append(util.manhattanDistance(newPos,ghost))
    if len(dist) > 0:
    	scored2 = min(dist)
    
    
    #3.
    dist2 = []
    scored3 = 0
    for eats in eat:
    	dist.append(util.manhattanDistance(newPos,eats))
    if len(dist2) > 0:
    	scored3 = min(dist2)
    	
    #Puntuacion total
    scored = 7*scored1-1*scored2-1*scored3
    
    return scored
    
   
   	
    
  
 
   

   
   
   
   
   
   
    
    
    
    
    
    
    
    
    
    
    #util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

